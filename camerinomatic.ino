/*
  CamerinoMatic 2.0
  
  An intervalometer and remote shutter for taking time lapses, selfies and other photos when connected to a DSLR with a proper connector.
  It is currently developed for testing on a Canon EOS Rebel T5 (1200).
  Currently, it uses only the shutter pin. Does not use the focus pin, so it does not send autofocus signal to the camera. The reason: I dont need it.
  
(C) 2015  Miguel Chavez Gamboa


Using an Arduino Pro Mini and Reed Relay for shotting. Operating on a couple of 1865 Lithum Batteries [in paralell] (from a "dead" laptop battery).
Development using an arduino Mega 2560.


MODOS DE OPERACION:

  - MANUAL. Solo se dispara si el boton REMOTE es pulsado.
  - RAFAGA. Igual que manual, pero con el shutter sin soltar por un tiempo configurable... la camara con el shutter en modo continuo.
  - BULB.   Se hace un disparo largo de acuerdo al tiempo seleccionado. el tiempo es el tiempo entre que se inicia el disparo y se finaliza (release shutter). La camara en modo BULB.
  - TIMELAPSE. Se hace una serie de disparos. Se usa el tiempo seleccionado para el tiempo total del timelapse.
      Se puede preguntar el num de disparos o el tiempo entre disparos, o ambos.
      Ejemplos: 
        * Quiero 1000 fotos en 5 horas. (Quiero 5 horas de timelapse con 1000 fotos.) Variables:  shots, totaltime. framerate=totaltime/shots
        * Quiero 2 horas de timelapse con disparos cada 10 minutos.  Variables: totaltime, framerate. shots=totaltime/framerate
        * Quiero 1000 fotos, una cada 20 segundos.
        
  
  *En los modos MANUAL y RAFAGA se presiona el boton START/REMOTE (RIGHT) para iniciar la foto (en RAFAGA se deja presionado el tiempo que se desee estar disparando).
  *En los modos BULB y TIMELAPSE despues de configurar el tiempo, para seleccionar num de disparos se presiona el boton SIGUIENTE (LEFT) y se usan los botones UP/DOWN,
  finalmente se presiona START/REMOTE para iniciar el proceso.
    
    
    Shutter     - Led 1  ||  Buzzer
    Timer tick  - Led 2  ||  Buzzer
    Battery LOW - Led 3  ||  Buzzer
 
Version 2.0: The hardware is new. First, the old LCD 16x2 is gone! Now I have an OLED display, monochrome (but first few rows are yellow and the others blue).
             And as a result of incorporating the new oled display, it may not fit on a pro mini.
             Only the Adafruit_GFX and Adafruit_SSD1306 libraries need 13 KB of precious flash space.

My chinese oled display. 
SPI version:

GND  VCC  CLK  MOSI  DC (Data/MISO)    CS (SS)
          to   Goes  DC goes to PIN    Goes to Slave Select
          PIN  to    50 on the MEGA.   which is PIN 53 on MEGA2560.
          52   PIN   This is the
               51    MISO pin.

*/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <Encoder.h>
Encoder myEnc(2, 3); //rotary encoder on pins 2,3 (4 for push button).
uint8_t oldPosition  = 0;

#define rotaryPushPin 4 //digital pin 4
uint8_t selector = 0;

#define OLED_MOSI 51
#define OLED_CLK 52
#define OLED_DC  50
#define OLED_CS  53
#define OLED_RESET 13 //NOT USED. My OLED display (SPI) does not have reset pin.
Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);


#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

#include <Timer.h>
Timer timer;

//MODES
#define MODE_MANUAL 0
#define MODE_BURST 1
#define MODE_TIMELAPSE 2
#define MODE_BULB 3
//DEFAULT VALUES
#define DEFAULT_TIME 60000L   //time in milliseconds. DEFAULT TIME = 1 minute (60,000 ms)
#define DEFAULT_SHOTS 10 //10 shots.
#define DEFAULT_SHOTTIME 150 //150 ms

#define SELECT_NONE 0
#define SELECT_TIME 1
#define SELECT_SHOTS 2
#define SELECT_RATE 3
#define SELECT_START 4

#define voltageSensorPin A15
#define shutter 22

#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)  
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN) 
#define numberOfHours(_time_) (( _time_% SECS_PER_DAY) / SECS_PER_HOUR)


#define TOGGLE_TITLE_TIME 5000 //5 seconds interval for checking title change.


byte get_center(String str, uint8_t font_size=1);

//PlayChar can be used to point the current variable that is being modified (or menu option that is being affected by the rotary encoder)
// For indicating the START action we need a bigger simbol.
static const unsigned char PROGMEM PlayChar[] = {
	0b00000,
	0b01000,
	0b01100,
	0b01110,
	0b01100,
	0b01000,
	0b00000,
	0b00000
};

String MODES_STR[4] = {"MANUAL", "RAFAGA", "TIME LAPSE", "BULB"}; //Strings
byte current_mode = 0;
byte title_timer;
//shutter timer for TL and BULB modes.
byte shutter_timer;
// time counting for updating the time left.
byte clock_timer;


boolean showVoltage = true;
String title_msg = "";

byte seconds; 
byte minutes;
byte hours;
byte minutes_frate;
byte seconds_frate;
byte hours_frate;

boolean remote;
boolean remote_burst;
boolean started;
int shots_done; //shots could be more than 1000. so we need 2bytes (cant use 1 byte).
int shots;
long frame_rate;


void setup()   {                
  
  Serial.begin(9600);
  
  pinMode(shutter, OUTPUT);
  pinMode(voltageSensorPin, INPUT);
  pinMode(2, INPUT); // rotary encoder
  pinMode(3, INPUT); // rotary encoder
  pinMode(rotaryPushPin, INPUT); //rotary encoder pushbutton pin
  //pinMode for the OLED Display is done at the Adafruit_GFX library, at the begin() method.
  
  hours = minutes = seconds = minutes_frate = seconds_frate = hours_frate = 0;
  current_mode = MODE_TIMELAPSE; // TODO: Load from eeprom later...
  selector = SELECT_NONE;
  remote = false;
  remote_burst = false;
  started = false;
  shots = shots_done = 0;
  frame_rate = 0L;
  
  //timers
  title_timer = timer.every(TOGGLE_TITLE_TIME, check_title, (void*)0);
  clock_timer = timer.every(1000, update_clock, (void*)0); //a one second timer.
  shutter_timer = -2; // not created here, it will be created when needed.
  
  //TESTING:
  hours = 0;
  minutes = 1;
  seconds = 0;
  minutes_frate = 0;
  seconds_frate = 10;
  hours_frate = 0;
  
  //DISPLAY SETUP
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC);
  
  // Clear the buffer.
  display.clearDisplay();
  
  // text display tests
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(30,0);
  display.println(F("CAMERINOMATIC"));
  display.setTextSize(2);
  display.println(F("    v2.0"));
  display.setTextSize(1);
  display.setCursor(0,30);
  display.println(F("      (C) 2015"));
  display.println(F("Miguel Chavez Gamboa"));
  display.println(F(""));
  display.println(F("     www.codea.me"));
  display.display();
  delay(3000);
}


void loop() {
  timer.update();
  
  //Rotary Encoder Stuff
  if (!started) {
    //reading push button
    delay(20); //wait,...
    int selBtn = digitalRead(rotaryPushPin);
    if ( !selBtn ) {
      if ( current_mode == MODE_TIMELAPSE && selector > SELECT_RATE) 
        selector = SELECT_NONE;
      else if ( (current_mode == MODE_MANUAL || current_mode == MODE_BURST) && selector >= SELECT_START)
        selector = SELECT_NONE;
      else if ( current_mode == MODE_BULB && selector > SELECT_RATE )
        selector = SELECT_NONE;
      else {
       if (current_mode == MODE_BULB && selector > SELECT_NONE)
          selector = SELECT_START;
        else 
       if ((current_mode == MODE_MANUAL || current_mode == MODE_BURST) && selector >= SELECT_NONE)
         selector = SELECT_START;
       else
        selector++;
      }
    }
    delay(50); //wait to dont change selector too fast.
  } //reading push button
  
  //reading encoder
  uint8_t newPosition = myEnc.read()/4; // encoder/library reports 4x ticks.
  
  //just inc mode when selector is 0, otherwise turning encoder should change other vars
  if ( selector == SELECT_NONE && !started) {
    if (newPosition > oldPosition) {
      current_mode++;
      if ( current_mode > SELECT_RATE)
        current_mode = MODE_MANUAL;
    } else if( newPosition < oldPosition ) {
      current_mode--;
      if ( current_mode > 254)
        current_mode = MODE_BULB;
    }
  }
  
  //checking if start OPTION is selected, allowing to start shots...
  if ( selector == SELECT_START ) {
    if ((current_mode == MODE_BULB || current_mode == MODE_MANUAL) && (newPosition < oldPosition) && !started) {
      //allow select this option by turning the rotary encoder to the left!
      Serial.println("STARTING BULB | MANUAL");
      remote = true;
      selector = SELECT_NONE;
    } else if (current_mode == MODE_TIMELAPSE && (newPosition < oldPosition) && !started ) {
      //allow select this option by turning the rotary encoder to the left!
      Serial.println("SART TIMELAPSE");
      remote = true;
      selector = SELECT_NONE;
    } else if (current_mode == MODE_BURST && (newPosition < oldPosition) && !started) {
      // BURST: We need to keep pressin a button to allow this mode. To stop (release shutter) we will use turning the knob to the right
      remote = true;
      remote_burst = true;
      selector = SELECT_START;
    } else if (current_mode == MODE_BURST && (newPosition > oldPosition) && started) {
      //finish burst mode, release shutter
      remote = false;
      remote_burst = false;
      started = false;
      selector = SELECT_NONE;
    }
  }
  
  // check for time/shots/rate up/down counting...
  if (!started) { //double check
    if ( newPosition > oldPosition) { //go up
      if (selector == SELECT_TIME)  seconds++;
      else if (selector == SELECT_SHOTS) shots++;
      else if (selector == SELECT_RATE) seconds_frate++; //frame_rate++;
    } else if (newPosition < oldPosition) { //go down
      if (selector == SELECT_TIME)  seconds--;
      else if (selector == SELECT_SHOTS) shots--;
      else if (selector == SELECT_RATE) seconds_frate--; //frame_rate--;
    }
    fix_time();
    fix_framerate();
    fix_shots();
  }
  
  //save new position for the next loop
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
  }
  
  //Now perform actions..
 
  switch(current_mode) {
    case MODE_MANUAL: {
      //check if the REMOTE button is pressed
      if (remote && !started) {
        do_shot(DEFAULT_SHOTTIME); //default shot time is 150 ms (if not changed at the def's).
        remote = false; //remove flag.
      }
      break;
    }
    case MODE_BURST: {
     //it seems this mode was not needed. the MANUAL mode do the same when keeping the remote button pressed, just fine tune the shutter time. 
     if ( !started ) 
       do_shot_burst(); //this opens and keep the shutter on until the REMOTE button is released.
     break;
    }
    case MODE_BULB: {
     //we need the time to keep the shutter on.
     if (!started && remote && (hours > 0 || minutes > 0 || seconds > 0)) {
       long period = hours*3600000L + minutes*60000L + seconds*1000L;
       shutter_timer = timer.pulse(shutter, period, HIGH); //TODO: Launch a shot now+10seconds, if perid is very long.
       Serial.println("STARTED="+String(started) +" TIME (ms) ="+String(period)+" timer->pinState:"+String(timer.getPinState(shutter_timer)));
       started = true;
       remote = false;
     }
     break;
    }
    case MODE_TIMELAPSE: {
     fix_framerate(); 
     if (!started && remote) {
       //CONSIDERING frame_rate is in milliseconds.
       //TODO: As now, the timer fires the first shot after first period has occurred. Shoud we do one shot right after starting timer (at second 0).
       //      for example, for a 5 minutes frame_rate, the first shot is at 5 minutes, the second at 10 minutes,etc.. if we start immediatly first shot should be at minute 0 second 0, the second should occurr at minute 5.
       if ( ( (hours > 0 || minutes > 0 || seconds > 0) && (shots > 0 ) ) ) {
         //case 1: given TIME and SHOTs number, calculate frame_rate
         long period = hours*3600000L + minutes*60000L + seconds*1000L;
         //long frame_rate = period/shots; //the time between each photo.
         shots_done = 0;
         shutter_timer = timer.every(period/shots, go_timelapse, shots, (void*)0);
         started = true;
         remote = false;
         Serial.println("STARTED="+String(started) +" TIME (ms) ="+String(period)+" Frame Rate:"+String(frame_rate)+ " SHOTS:"+String(shots)  );
       } else
       if ( ( (hours > 0 || minutes > 0 || seconds > 0) && (frame_rate > 0 ) ) ) {
         //case 2: given TIME and frame_rate, calculate shots.
         long period = hours*3600000L + minutes*60000L + seconds*1000L;
         fix_framerate();
         shots = period/frame_rate;
         shots_done = 0;
         shutter_timer = timer.every(frame_rate, go_timelapse, shots, (void*)0);
         started = true;
         remote = false;
         Serial.println("STARTED="+String(started) +" TIME (ms) ="+String(period)+" Frame Rate:"+String(frame_rate)+ " SHOTS:"+String(shots)  );         
       } else 
       if ( shots > 0 && frame_rate > 0 ) {
         //case 3: given shots and frame_rate, calculate TIME. TIME var (hour,min,sec ar invalid or ZERO)
         fix_framerate();
         long period = frame_rate*shots; //ESTABA COMENTADO. Solo es para el debug
         //pass period time to hours,min,sec.
         hours = numberOfHours(period/1000);
         minutes = numberOfMinutes(period/1000);
         seconds = numberOfSeconds(period/1000);
         shots_done = 0;
         shutter_timer = timer.every(frame_rate, go_timelapse, shots, (void*)0);
         started = true;
         remote = false;
         Serial.println("STARTED="+String(started) +" TIME (ms) ="+String(period)+" Frame Rate:"+String(frame_rate)+ " SHOTS:"+String(shots)  );         
       }
     }//if !started && remote
     break;
    }
  } //switch MODE
  
  //started notifier
  if (started) {
    int sstate = timer.getPinState(shutter_timer);
    //DO A NOTIFIER FOR THE OLED DISPLAY. A circle filling according to time
    Serial.println("STARTED:"+String(started)+" REMOTE:"+String(remote)+"  SHUTTER VAL:"+String(sstate));
    //digitalWrite(led, sstate);
  }
  
  //DEBUG ONLY
  //Serial.println("MODE:"+MODES_STR[current_mode]+" Rotary Pos: "+String(newPosition)+" SEL: "+String(selector));
  
  //reset start flag when finished shutter timer
  if (started && !timer.getPinState(shutter_timer) && current_mode == MODE_BULB) {
    started = false;
    remote = false; 
    seconds = 0; //reset time for clock updating
  }
  display.clearDisplay();
  print_header();
  print_time();
  print_tlVars();
  print_startBtn();
  display.display(); //update display.
  
  //Serial.println("ShutterTimer->pinState:"+String(timer.getPinState(shutter_timer)) + "STARTED:"+String(started));
}


/* Prints the voltage/title in first row, and MODE on the second row. */
void print_header() {
  delay(20);
  int battery_voltage = map(analogRead(voltageSensorPin), 0, 1023, 0, 5000); //voltage in mV.
  display.setTextSize(1);
  if (showVoltage) {
      /*
          Battery charge levels ===============
          4.2  volts => 100% charge.
          3.9  volts => 75% charge.
          3.81 volts => 50% charge.  ==> NOMINAL CHARGE
          3.70 volts => 25% charge.
          3.65 volts => 12.5% (1/8) charge. ALARM, CHARGE NEEDED!
          3.60 volts =>  DISCHARGED, charge NOW.
    */
    String str_voltage = String(battery_voltage).substring(0,1) +"."+ String(battery_voltage).substring(1,3) + "v";
    byte percent = 0;
    if ( battery_voltage > 3899 ) 
      percent = 100;
    else if ( battery_voltage > 3799 )
     percent = 75;
    else if ( battery_voltage > 3699 )
      percent = 50;
    else if ( battery_voltage > 3649 )
      percent = 25;
    else if ( battery_voltage > 3600 )    
      percent = 12;
    else if ( battery_voltage <= 3600 ) 
      percent = 2;
    display.setCursor(0,0);
    display.print(String(percent)+"%");
    display.setCursor(98,0);
    display.print(str_voltage);
  } else {
    if ( battery_voltage <= 3650) {
      //display an alarm to charge battery!
      String msg = "BATERIA BAJA!";
      display.setCursor(get_center(msg), 0);
      display.print(msg);
    } else {
    display.setCursor(15,0);
    display.print(F("CAMERINOMATIC 2.0"));
    }
  }
  
  if ( selector == SELECT_NONE ) { //We are selecting MODE so mark mode
    //invert display
    if ( !started )
      display.setTextColor(BLACK,WHITE);
    else
      display.setTextColor(WHITE,BLACK);
  } else {
    display.setTextColor(WHITE,BLACK);
  }
  byte center = get_center(MODES_STR[current_mode]);
  display.setCursor(center, 9);
  display.println(MODES_STR[current_mode]);
  display.setTextColor(WHITE,BLACK); //reinvert display...
}


byte get_center(String str, uint8_t font_size) {
  Serial.println("FONT_SIZE: "+String(font_size));
  byte r = (str.length()*6*font_size)/2; // r alone is in number of chars, but setCursor is in pixels.. hardcoding *5 is taking into account that font size is 1 (about 5 pixels wide font)
  //Serial.println("Center for "+ str + " " + String(64-r) + " r= "+String(r));
  return (64 - r);// put it in the center of the screen.
}

void check_title(void *context) {
 showVoltage = !showVoltage;
}

void print_time(){
  if (current_mode == MODE_BULB || current_mode == MODE_TIMELAPSE) {
    if (!started) {
      //modifying settings
      display.setCursor(0,22);
      if (selector == SELECT_TIME)
        display.setTextColor(BLACK,WHITE); //inverted
      else
        display.setTextColor(WHITE,BLACK);
      display.println("LAP: "+format_time() );
      display.setTextColor(WHITE,BLACK);
    } else {
      //STARTED. So we change things to display!
      display.setCursor(get_center(format_time(),2), 22);
      display.setTextSize(2);
      display.setTextColor(WHITE,BLACK);
      display.println(format_time());
      display.setTextSize(1);
    }
  } else {
    //Not in BULB or T.L. - We dont need timers.
  }
}

void print_tlVars() {
  display.setCursor(0,32);
  if (current_mode == MODE_TIMELAPSE && !started) {
    if (selector == SELECT_SHOTS)
      display.setTextColor(BLACK,WHITE); //inverted
    else
      display.setTextColor(WHITE,BLACK);
    //first, display # of shots
    display.print("# of Shots: "+String(shots));
    if (selector == SELECT_RATE)
      display.setTextColor(BLACK,WHITE); //inverted
    else
      display.setTextColor(WHITE,BLACK);
    //display frame rate var..
      display.setCursor(0,42);
    display.print("Shot LAP: "+ format_frate());  
  } else if (current_mode == MODE_TIMELAPSE && started) {
  //Display number of shots done and left.
    String msg = String(shots_done) +"/"+ String(shots) +" Fotos";
    display.setCursor(get_center(msg,1), 46);
    display.setTextSize(1);
    display.setTextColor(WHITE,BLACK);
    display.print(msg);
  } else if (current_mode == MODE_BULB && started) {
    display.setCursor(get_center("Bulb",2), 46);
    display.setTextSize(2);
    display.setTextColor(WHITE,BLACK);
    display.print("Bulb");
  } else if (current_mode == MODE_BURST && started) {
    display.setCursor(get_center("Burst",2), 46);
    display.setTextSize(2);
    display.setTextColor(WHITE,BLACK);
    display.print("Burst");
  }
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
}

void print_startBtn() {
  if (!started) {
    display.setCursor(45,56);
    if ( selector == SELECT_START ) 
      display.setTextColor(BLACK,WHITE);
    else
      display.setTextColor(WHITE,BLACK);
    display.println(F("INICIAR"));
    display.setTextColor(WHITE,BLACK);
  }
}

String format_time(){
  //fix time strings
  String str_hrs, str_min, str_sec;
  if (hours < 10)
    str_hrs = "0"+String(hours);
  else
    str_hrs = String(hours);
  if (minutes < 10)
    str_min = "0"+String(minutes);
  else 
    str_min = String(minutes);
  if (seconds < 10)
    str_sec = "0"+String(seconds);
  else
    str_sec = String(seconds);
  
  return str_hrs+":"+str_min+":"+str_sec;
}

String format_frate(){
  //fix time strings
  String str_hrs, str_min, str_sec;
  if (hours_frate < 10)
    str_hrs = "0"+String(hours_frate);
  else
    str_hrs = String(hours_frate);
  if (minutes_frate < 10)
    str_min = "0"+String(minutes_frate);
  else 
    str_min = String(minutes_frate);
  if (seconds_frate < 10)
    str_sec = "0"+String(seconds_frate);
  else
    str_sec = String(seconds_frate);
  
  return str_hrs+":"+str_min+":"+str_sec;
}

void fix_time() {
  //This method increment the hours,minutes,seconds variables when a UP or DOWN button is pushed.
  //The seconds variable is incremented on the method that polls the push buttons. Then this method is called there.
  
  ///Serial.println(String(hours)+":"+String(minutes)+":"+String(seconds));
  
  //fix when decrementing
  if (seconds > 60) {  //seconds < 0 ( bytes are from 0 to 255. so 0-1 = 255. But when on FASTFORWARD it could be 35-255=231 so we check >60)
    seconds = 59;  
    minutes--;
  }
  if (minutes > 60) {
    minutes = 59;
    hours--;
  }
  if (hours > 60) {
    hours = 23;
  }
  
  //fix when incrementing
  if (seconds > 59) {
    seconds = 0;  
    minutes++;
  }
  if (minutes > 59) {
    minutes = 0;
    hours++;
  }
  if (hours > 23) {
    hours = 0;
    minutes = 0;
    seconds = 0;
    //What to do? drop the increment silently or inform? a pulse on a buzzer maybe
    //We do not support more than 24 hours..
  }
  
}

void fix_framerate() {
  //frame_rate contains or will contain a number in milliseconds.
  // seconds_frate contain seconds part of frame_time, minutes contain minutes part of frame_time.
  
  //fix when decrementing
  if (seconds_frate > 60) { 
    seconds_frate = 59;  
    minutes_frate--;
  }
  if (minutes_frate > 60) {
    minutes_frate = 59;
    hours_frate--;
  }
  if (hours_frate > 60) {
    hours_frate = 23;
  }
  
  //fix when incrementing
  if (seconds_frate > 59) {
    seconds_frate = 0;  
    minutes_frate++;
  }
  if (minutes_frate > 59) {
    minutes_frate = 0;
    hours_frate++;
  }
  if (hours_frate > 23) {
    hours_frate = 0;
    minutes_frate = 0;
    seconds_frate = 0;
    //What to do? drop the increment silently or inform? a pulse on a buzzer maybe
    //We do not support more than 24 hours..
  }
  
  frame_rate = hours_frate*3600000L + minutes_frate*60000L + seconds_frate*1000L; //update frame rate according to given time from user.
}


void fix_shots() {
  if (shots < 0) 
    shots = 150; //going to a bigger value instead of stay at 0. like a circular, but not within upper limits.
}


void update_clock(void *context) {
  // IT seems not accurate. This inacuracy seems to be due to the delay() inside go_timelapse() method and other delays in the loop. 
  // One problem with this apporach is that the time variables is reset to 0, and if we need to take another TL we need to enter time again which could be time consuming (or reset the board?)
  // One fix is to save the time to eeprom and reload on finish (also for reload on restart).
  
  if (started && (current_mode == MODE_TIMELAPSE || current_mode == MODE_BULB)) {
    seconds--;
    fix_time();
  } //if started
}

void go_timelapse(void *context) {
  do_shot(100); //do_shot uses delay.
  shots_done++;
  //Serial.println("Shots done: "+String(shots_done)+" de "+String(shots));
  if (shots_done >= shots) {
    //Serial.println("TL DONE, "+String(shots_done)+ " de "+String(shots)+" fotos tomadas.");
    started = false;
    selector = 0; //reset selector when finishing TL.
    seconds = 0; //reset time for clock updating
  }
}

void do_shot(int time) {
  //TODO: Do this with millis()/timer instead of delay()
  //NOTE: Do not add started = true/false because it breaks things! using it from go_timelapse().
  digitalWrite(shutter, HIGH);
  delay(time);
  digitalWrite(shutter,LOW);
}

void do_shot_burst(){
  started = remote_burst;
  if (remote_burst) {
    digitalWrite(shutter, HIGH);
    //continue shoting no release shutter...
  } else {
    //now release shutter.
    digitalWrite(shutter,LOW);
    remote = false;
    started = false;
  }
}


